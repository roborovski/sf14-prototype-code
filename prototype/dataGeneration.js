function generateTags(numberOfTags, maxNumberOfNewRelatedTags) {
	var tags = {};
	var availableTags = [];
	
	var nameLengthMax = ((numberOfTags - 1) + "").length;
	var name;
	var i, j, n;
	for (i = 0; i < numberOfTags; i++) {
		name = ("000000000" + i).slice(-nameLengthMax);
		tags[name] = {name: name, relations: []};
		availableTags.push(name);
	}

	var relations, relatedTag, numberOfRelatedTags, canUseTag;
	for (tag in tags) {
		relations = [];
		numberOfRelatedTags = Math.round(Math.random() * maxNumberOfNewRelatedTags);
		for (j = 0; j < numberOfRelatedTags; j++) {
			do {
				canUseTag = false;
				relatedTag = availableTags[Math.floor(Math.random() * availableTags.length)];
				for (n = 0; n < relations.length; n++) {
					if (relatedTag == relations[n].name || relatedTag == tag) canUseTag = true;
				}
			} while (canUseTag)
			var relationStrength = Math.random() * Math.random();
			relations.push({
				name: relatedTag,
				relationStrength: relationStrength
			});
			tags[relatedTag].relations.push({
				name: tag,
				relationStrength: relationStrength
			});
		}
		tags[tag].relations = tags[tag].relations.concat(relations);
	}
	return tags;
}
		
function generateMaterials(numberOfMaterials, tags) {
	var materials = [];
	var titles = ["Getting your Head Around", "Jump-Start", "Get Started With", "How to Program Using", "Get Into", "The Manual for", "The Definitive Guide to", "Getting Ahead in", "Mastering", "Professional", "Business-grade",  "Security in", "Secure", "Thinking in", "Up to Speed with", "Hardcore", "Integrated", "Practical", "How to Effectively", "Nirvana with", "Becoming an Expert in", "Learn Super Duper", "The Definitive Reference for"];
	var descriptionSentences = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Duis eget diam in massa molestie blandit.", "Phasellus a lacinia enim.", "Donec vestibulum ac urna ut cursus.", "Etiam condimentum porta semper.", "Phasellus sit amet velit interdum, pretium dui ac, facilisis enim.", "Nullam feugiat purus eget congue dictum.", "Donec cursus luctus eros, at volutpat massa ullamcorper eget.", "Aliquam erat volutpat.", "Cras vehicula a est nec cursus.", "Fusce sed ante pharetra, vestibulum mi quis, scelerisque quam.", "Morbi sit amet lacus erat.", "Praesent eu blandit nibh.", "Pellentesque consectetur eu felis vitae commodo.", "Vestibulum tincidunt orci in dolor dictum iaculis.", "Nam ac varius neque, ac feugiat risus.", "Aenean a semper leo.", "Aliquam accumsan tincidunt aliquam.", "Phasellus at magna interdum arcu vestibulum mollis non in lectus.", "Nam ut neque vitae nisl condimentum accumsan vel et tortor.", "Nulla id lobortis felis.", "Sed sed tincidunt nisl. Curabitur sapien mi, rutrum in accumsan ac, sollicitudin elementum nunc.", "Sed non turpis id ipsum rutrum eleifend.", "Morbi suscipit eleifend mauris, sodales volutpat velit fermentum at.", "In blandit, tellus quis accumsan adipiscing, mi leo tincidunt nunc, ut sollicitudin nulla diam in tellus.", "Ivamus a ligula in urna ornare feugiat.", "Nullam quam orci, lacinia quis nisl eu, consectetur commodo urna.", "Curabitur eget augue porttitor, placerat quam sit amet, interdum nisl."];
	var firstNames = ["Eric", "Brook", "Josie", "Wen", "Camellia", "Kena", "Alberta", "Kymberly", "Serita", "Torrie", "Krystin", "Rebecca", "Enrique", "Marna", "Rod", "Sherwood", "Genia", "Dodie", "Rosena", "Adella", "Toby", "Latonia", "Fumiko", "Talitha", "Deloise", "Bernadine", "Emilee", "Nidia", "Dorothy", "Yetta", "Elba", "Tambra", "Hwa", "Pamila", "Gianna", "Caron", "Gena", "Angella", "Krishna", "Candra", "Janis", "Laurine", "Haywood", "Gillian", "Rick", "Victoria", "Warren", "Felisa", "Joleen", "Crystal"];
	var middleInitials = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var lastNames = ["Palato", "Restic", "Vize", "Jupp", "Danz", "Mulloy", "Skerry", "Crowley", "Henderson", "Guarnieri", "Malmstad", "Welles", "Merminod", "Dorra", "Valminuto", "Stukel", "Bauer", "Chiu", "Genova", "Spera", "Marcillo", "Sorensen", "Shuppert", "Onley", "Wentz", "Doering", "Aklog", "Partington", "Slack", "England", "Salway", "Shea-simonds", "Dell'antonio", "Morrissey", "Scheer", "Peebles", "Dole", "Defranco", "Prelich", "Schwalenberg", "North", "Schofield", "Alt", "Nunez-farfar", "Fischbach", "Tingle", "Disraeli", "Ngo", "Wigotsky", "Prusa", "Peled", "Soule", "Champagne", "Baglivo", "Midhet", "Ghorai", "Esparza", "Pearlberg", "Quade", "Mcgee", "Robertucci", "Demaio", "Goodridge", "Suzuki", "Shimin", "Liest", "Heifetz", "Brockenbrough", "Pallant", "Manalis", "Boezi", "Barth", "Naylor", "Halverson", "Tawn", "Liander", "Devellis", "Geissler", "Hagen", "Gelbart", "Hunink", "Wilcoxon", "Valois", "Cassels", "Levasseur", "Koerner", "Bichel", "De l'enclos", "Karasik", "Philp", "Loret", "Katz", "Mahjub", "Glenn", "Lovell", "Durocher", "Kumins", "Cotreau", "Davidson", "Petkau", "Glynn"];
	
	var availableTags = 0;
	for (tag in tags) {availableTags++;}

	var tagNameLengthMax = ((availableTags - 1) + "").length;

	var lowestPossibleYear = 1939;
	for (var i = 0; i < numberOfMaterials; i++) {
		var material = {};

		// generate name
		material.author = {
			firstName: firstNames[Math.floor(firstNames.length * Math.random())],
			middleInitial: middleInitials.charAt(Math.round(middleInitials.length * Math.random())),
			lastName: lastNames[Math.floor(firstNames.length * Math.random())]
		};

		// generate date
		var month = Math.ceil(Math.random() * 12);
		var day = Math.ceil(Math.random() * 30);
		var year = lowestPossibleYear + Math.round(Math.random() * (2013 - lowestPossibleYear));
		material.date = month + "/" + day + "/" + year;

		// generate tags
		var relatedTags = [];
		var rootTag = tags[("000000000" + Math.floor(availableTags * Math.random())).slice(-tagNameLengthMax)];
		relatedTags.push({
			name: rootTag.name,
			relationStrength: 1
		});
		relatedTags = relatedTags.concat(rootTag.relations);
		material.relatedTags = relatedTags;

		// generate title
		material.title = titles[Math.floor(titles.length * Math.random())] + " " + rootTag.name;
		
		// generate description
		var descriptionSentencesIndexes = [];
		while (descriptionSentencesIndexes.length < 8) {
			var index = Math.floor(descriptionSentences.length * Math.random());
			var indexFound = false;
			for (var j = 0; j < descriptionSentencesIndexes.length; j++) {
				if (descriptionSentencesIndexes[j] == index) {
					indexFound = true;
					break;
				}
			}
			if(!indexFound) descriptionSentencesIndexes.push(index);
		}
		var description = "";
		for (var j = 0; j < descriptionSentencesIndexes.length; j++) {
			var sentence = descriptionSentences[descriptionSentencesIndexes[j]];
			description += sentence + (j == descriptionSentencesIndexes.length - 1 ? "" : " ");
		}
		material.description = description;

		// add material to list
		materials.push(material);
	}
	return materials;
}