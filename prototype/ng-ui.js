if (Modernizr.draganddrop) {
  // Browser supports HTML5 DnD.
} else {
	alert("No drag and drop! Oh no!");
}

function createCards(numberOfCards){
	
}

var dropElements = document.querySelectorAll('.well-row .well');
var dragElements = document.querySelectorAll('.card-row .card');
var elementDragged = null;

var completedCards = 0;


var validPlace = false;

function dragBegin(e) {
	e.dataTransfer.effectAllowed = 'move';
	e.dataTransfer.setData('text', this.innerHTML);
	$('.well-row .well').addClass('dotted');
	elementDragged = this;
}

for (var i = 0; i < dragElements.length; i++) {

	// Event Listener for when the drag interaction starts.
	dragElements[i].addEventListener('dragstart', dragBegin, false);

	// Event Listener for when the drag interaction finishes.
	dragElements[i].addEventListener('dragend', function(e) {
		$('.well-row .well').removeClass('dotted');
	});
};

for (var i = 0; i < dropElements.length; i++) {
	dropElements[i].addEventListener('dragover', function(e) {
		if (e.preventDefault) {
			e.preventDefault();
		}

		e.dataTransfer.dropEffect = 'move';

		return false;
	});

	dropElements[i].addEventListener('dragenter', function(e) {	
		validPlace = true;
	});
	dropElements[i].addEventListener('dragleave', function(e) {	});
	dropElements[i].addEventListener('drop', function(e) {
		if (e.preventDefault) e.preventDefault(); 
	  	if (e.stopPropagation) e.stopPropagation(); 
	  	var cardContents = e.dataTransfer.getData('text');

		this.innerHTML = cardContents;
		this.className = "panel panel-primary card";
		// Remove the element from the list.
		if (validPlace == true) {
			elementDragged.innerHTML = "<i class=\"fa fa-check checkmark\"></i>";
			elementDragged.className = "well";
			this.input += cardContents;
			completedCards++;
			elementDragged.removeEventListener('dragstart',dragBegin, false);
			elementDragged.draggable = false;
		};
		elementDragged = null;
		dragElements
		return false;
	});
};


// function buildCard(cardName){

// 	var selectedMaterial = finalMaterials[material];
// 	// get card
// 	var card = document.getElementById(material + "Item");
// 	// get/set description
// 	var description = $(card).children(".description");
// 	$(description).html(
// 		//add icon stuff
// 		"<i>" + selectedMaterial.description +"</i>"
// 		);
// 	// get/set title
// 	var title = $(card).children(".panel-heading");
// 	$(title).html(selectedMaterial.title);
// }


//set canvas up for displaying material

var dataDisplay = $("#dataDisplay");
var dataDisplayCanvas = $("#dataDisplayCanvas");


var canvas = dataDisplayCanvas[0];
var context = canvas.getContext("2d");

var minWidth = 800;
var minHeight = 600;


// generate tags
var tagCount = 100;
var maxNewTagRelations = 15;
var tags = generateTags(tagCount, maxNewTagRelations);

// generate materials
var materialCount = 3;

//generate material
var materials = generateMaterials(5000, tags);

function displayMaterial(materialID, dontClear) {
	var material = materials[materialID];
	if (material == undefined) {
		console.log("oh no! no material!");
		return;
	}

	
	context.clearRect(0, 0, canvas.width, canvas.height);
	
	if (dontClear == undefined) dontClear = false;
	if (!dontClear) {
		dataDisplay.html("<div class=\"material\"></div>");
	} else {
		dataDisplay.append("<div class=\"material\"></div>");
	}
	var materialPanel = dataDisplay.find(".material").last();


	materialPanel.append("<h1>" + material.title + "</h1>");
	materialPanel.append("<p></p>");

	var materialPanelParagraph = materialPanel.find("p").eq(0);

	var author = material.author;
	author = author.firstName + " " + author.middleInitial + ". " + author.lastName;
	materialPanelParagraph.append("Author name: <span class=\"info\">" + author + "</span><br />");
	materialPanelParagraph.append("Year published: <span class=\"info\">" + material.date + "</span>");
	materialPanel.append("<p class=\"lighten\">" + material.description + "</p>");		

	materialPanel.append("<h1>Related Tags</h1>");
	materialPanel.append("<p></p>");
	materialPanelParagraph = materialPanel.find("p").eq(2);

	var relatedTags = material.relatedTags;
	relatedTags.sort(function(a, b) {
		return b.relationStrength - a.relationStrength;
	});
	for (var i = 0; i < relatedTags.length; i++) {
		materialPanelParagraph.append("<a href=\"javascript: void(0);\">" + relatedTags[i].name + "</a>&nbsp;");
		var tag = materialPanel.find("a").eq(i);
		tag.css("font-size", (12 + relatedTags[i].relationStrength * 12) + "px");
	};

}
displayMaterial(50, true);

function popupAnswer(){
	if (completedCards >= dropElements.length) {
		$('#myModal').modal();
	} else {
		alert("please file all cards. Thanks.");
	}
}